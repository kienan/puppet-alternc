# alternc

## Dependencies

 * puppetlabs/apt

## Setup

## Limitations

Currently just manages the basic install, and adds the AlternC
upstream source if you want.

## Example Usage

Make sure the database alternc is created and the users alternc
and alternc_user have access to it. Pass their passwords into the
class as follows.

`
  class { 'alternc::bureau':
    database_password      => 'something',
    database_mail_password => 'something_else',
  }
`

## Reference

## Copyright & License

Copyright 2018 Kienan Stewart <kienan.stewart@burntworld.ca>

Inspired and following on from the manifest started for [alternc-vagrant][1]

Licensed under GPLv3 or later, see LICENSE for full text.

[1]: https://github.com/lelutin/alternc-vagrant
