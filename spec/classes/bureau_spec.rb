require 'spec_helper'
describe 'alternc::bureau' do
  let(:title) { 'alternc' }
  let(:params) {
    {
      'database_password' => 'asdf',
      'database_mail_password' => 'asdf',
    }
  }
  # Need to fill out a fair amounts of facts for the apt module.
  let(:facts) {
    {
      :operatingsystem => 'Debian',
      :osfamily => 'Debian',
      :os => {
        'name' => 'Debian',
        'lsb' => {
          'distcodename' => 'stretch',
          'majdistrelease' => '9'
        },
        'release' => {
          'full' => '',
          'major' => '9'
        }
      },
      :ipaddress => '10.0.0.2',
    }
  }

  it do
    is_expected.to contain_file('/var/lib/dpkg/alternc.response')
    is_expected.to contain_package('alternc').with(
      'ensure' => 'present',
      'name'   => 'alternc'
                   )
    is_expected.to contain_class('alternc::source')
  end

  context 'with manage_sources => false' do
    let(:params) {
      {
        'database_password' => 'asdf',
        'database_mail_password' => 'asdf',
        'manage_sources' => false
      }
    }
    it { is_expected.not_to contain_class('alternc::source') }
  end
end
