class alternc::bureau (
  String $database_password,
  String $database_mail_password,
  String $panel_title = 'AlternC',
  String $panel_hostname = $facts['fqdn'],
  String $panel_internal_ip = $facts['ipaddress'],
  String $panel_public_ip = $facts['ipaddress'],
  Boolean $panel_use_private_ip = true,
  String $mail_directory = '/var/mail/alternc',
  String $html_directory = '/var/www/alternc',
  String $logs_directory = '/var/log/alternc/sites',
  Array[String] $nameservers = [
    $facts['hostname']
  ],
  Array[String] $mx = [
    $facts['hostname']
  ],
  String $database_host = 'localhost',
  String $database_name = 'alternc',
  String $database_user = 'alternc',
  String $database_mail_user = 'alternc_user',
  Boolean $database_is_local = true,
  Boolean $manage_sources = true,
) {
  $ns_primary = $nameservers[0]
  if $nameservers[1] == undef {
    $ns_secondary = $ns_primary
  }
  else {
    $ns_secondary = $nameservers[1]
  }
  $mx_primary = $mx[0]
  if $mx[1] == undef {
    $mx_secondary = ''
  }
  else {
    $mx_secondary = $mx[1]
  }
  file { '/var/lib/dpkg/alternc.response':
    ensure  => present,
    content => template('alternc/alternc.response.erb'),
  }
  if $manage_sources {
    include alternc::source
  }
  package { 'acl':
    ensure => present,
  }
  package { 'alternc':
    ensure       => present,
    responsefile => '/var/lib/dpkg/alternc.response',
    require      => Package['acl'],
  }

  exec { 'alternc.install':
    command     => '/usr/sbin/alternc.install',
    refreshonly => true,
  }

  exec { 'alternc.restartapache':
    command     => '/usr/sbin/service apache2 restart',
    refreshonly => true,
  }

  File['/var/lib/dpkg/alternc.response']
  -> Package['alternc']
  ~> Exec['alternc.install']
  ~> Exec['alternc.restartapache']

  # Configuration to be updated once AlternC is already installed
  # changing the preseed post-installation doesn't help immediately
  # so we ensure relevant configuration values in /etc/alternc/local.sh
  alternc::conf::local { 'HOSTING':
    value => $panel_title
  }
  alternc::conf::local { 'FQDN':
    value => $panel_hostname,
  }
  alternc::conf::local { 'PUBLIC_IP':
    value => $panel_public_ip,
  }
  alternc::conf::local { 'INTERNAL_IP':
    value => $panel_internal_ip,
  }
  alternc::conf::local { 'NS1_HOSTNAME':
    value => $ns_primary,
  }
  alternc::conf::local { 'NS2_HOSTNAME':
    value => $ns_secondary,
  }
  alternc::conf::local { 'DEFAULT_MX':
    value => $mx_primary,
  }
  alternc::conf::local { 'DEFAULT_SECONDARY_MX':
    value => $mx_secondary,
  }
  alternc::conf::local { 'ALTERNC_HTML':
    value => $html_directory,
  }
  alternc::conf::local { 'ALTERNC_MAIL':
    value => $mail_directory,
  }
  alternc::conf::local { 'ALTERNC_LOGS':
    value => $logs_directory,
  }
}
