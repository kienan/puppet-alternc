define alternc::domain_type (
  $ensure = 'present',
  String $type_name = $name,
  String $description = '',
  String $target = 'DOMAIN',
  String $entry = '%SUB% IN A @@PUBLIC_IP@@',
  String $compatibility = 'txt,defmx,defmx2,mx,mx2',
  String $enable_for = 'ALL',
  Boolean $only_dns = false,
  Boolean $need_dns = false,
  Boolean $advanced = false,
  Boolean $create_tmpdir = true,
  Boolean $create_targetdir = true,
  Boolean $has_https_option = false,
  Boolean $manage_template = true,
  String  $template_path = '',
  String  $template_content = '',
) {
  $mysql_command = '/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf'
  $command_set = "${mysql_command} -e \"
update domaines_type set name = '${type_name}', description = '${description}',
target = '${target}', entry = '${entry}', compatibility = '${compatibility}',
enable = '${enable_for}', only_dns = ${only_dns}, need_dns = ${need_dns},
advanced = ${advanced}, create_tmpdir = ${create_tmpdir},
create_targetdir = ${create_targetdir}, has_https_option = ${has_https_option}
where name = '${type_name}';
\""

  $command_exists = "${mysql_command} -e \"select name from domaines_type where name = '${type_name}'\\G;\" | grep -q 'name: ${type_name}'"

  $command_remove = "${mysql_command} -e \"delete from domaines_type where name = '${type_name}'\""

  $command_is = "${mysql_command} -e \"select name from domaines_type where name = '${type_name}' and description = '${description}' and target = '${target}' and entry = '${entry}' and compatibility = '${compatibility}' and enable = '${enable_for}' and only_dns = ${only_dns} and need_dns = ${need_dns} and advanced = ${advanced} and create_tmpdir = ${create_tmpdir} and create_targetdir = ${create_targetdir} and has_https_option = ${has_https_option}\G;\" | grep -q 'name: ${type_name}'"

  $command_add = "${mysql_command} -e \"insert into domaines_type (name, description, target, entry, compatibility, enable, only_dns, need_dns, advanced, create_tmpdir, create_targetdir, has_https_option) VALUES ('${type_name}', '${description}', '${target}', '${entry}', '${compatibility}', '${enable_for}', ${only_dns}, ${need_dns}, ${advanced}, ${create_tmpdir}, ${create_targetdir}, ${has_https_option});\""

  $real_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'present'
  }

  if $real_ensure == 'present' {
    exec { "alternc_domain_type_set_${name}":
      command => $command_set,
      unless  => $command_is,
      require => Package['alternc'],
    }
    exec { "alternc_domain_type_add_${name}":
      command => $command_add,
      unless  => $command_exists,
      require => Package['alternc'],
    }
  }
  else {
    # This could monstrously mess up AlternC if a domain type that is in use
    # is removed.
    exec { "alternc_domain_type_remove_${name}":
      command => $command_remove,
      onlyif  => $command_exists,
      require => Package['alternc'],
    }
  }
  if $manage_template {
    file { "alternc_domain_type_${name}":
      ensure  => $real_ensure,
      path    => $template_path,
      content => $template_content,
      owner   => 'root',
      group   => 'root',
      mode    => '0644',
    }
  }
}
