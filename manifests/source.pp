class alternc::source {
  # Upstream hasn't released anything into the stretch archives
  # or later. 2018.06.08
  if ($facts['os']['lsb']['majdistrelease'] >= '8') {
    $release = 'jessie'
  }
  else {
    $release = $facts['os']['lsb']['distcodename']
  }
  apt::source { 'alternc':
    location => 'http://debian.alternc.org',
    release  => $release,
    key      => {
      id     => '54E9B3EC75A6375C4670104BA4AFB8B58971F6F2',
      source => 'https://debian.alternc.org/key.txt',
    },
    repos    => 'main',
  }
  Apt::Source['alternc']
  -> Package['alternc']
  Apt::Source['alternc']
  ~> Class['apt::update']
}
