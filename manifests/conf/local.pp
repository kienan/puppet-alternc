define alternc::conf::local (
  $ensure  = 'present',
  $key     = $name,
  $value   = '',
  $match   = undef,
) {
  if !$match {
    $_match = "${key}=.*"
  } else {
    $_match = $match
  }
  file_line { "${name}_local.sh_line":
    ensure  => $ensure,
    path    => '/etc/alternc/local.sh',
    line    => "${key}=${value}",
    match   => $_match,
    require => Package['alternc'],
  }
}
