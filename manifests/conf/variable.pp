# Sets values in the variable table of the alternc database
define alternc::conf::variable (
  $ensure = 'present',
  $key    = $name,
  $value  = '',
) {
  $command_set = "/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf -e \"update variable set value = '${value}' where name = '${key}';\""
  $command_exists = "/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf -e \"select value from variable where name = '${key}';\" | grep -q value"
  $command_remove = "/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf -e \"delete from variable where name = '${key}'\""
  $command_is = "/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf -e \"select value from variable where name = '${key}';\" | grep -q ${value}"
  $command_add = "/usr/bin/mysql --defaults-file=/etc/alternc/my.cnf -e \"insert into variable (name, value) values ('${key}', '${value}');\""
  $real_ensure = $ensure ? {
    'absent' => 'absent',
    default  => 'present'
  }

  if $real_ensure == 'present' {
    exec { "alternc_variable_set_${name}":
      command => $command_set,
      unless  => $command_is,
      require => Package['alternc'],
    }
    exec { "alternc_variable_add_${name}":
      command => $command_add,
      unless  => $command_exists,
      require => Package['alternc'],
    }
  }
  else {
    exec { "alternc_variable_remove_${name}":
      command => $command_remove,
      onlyif  => $command_exists,
      require => Package['alternc'],
    }
  }
}
